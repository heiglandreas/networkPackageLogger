package main

import (
  "os"
  "fmt"
  "github.com/google/gopacket"
  "github.com/google/gopacket/layers"
  "github.com/google/gopacket/pcap"
  "log"
  "time"
  "database/sql"
  _ "github.com/lib/pq"
  "encoding/json"
)

var (
    device      string = "en0"
    snapshotLen int32  = 1024
    promiscuous bool   = true
    err         error
    timeout     time.Duration = 30 * time.Second
    handle      *pcap.Handle
)

type Config struct {
  DBUser string
  DBPass string
  DBName string
  Device string
}

func main() {
    config := ReadConfig("./networkPackageLogger.json")

    db := OpenDB(config)
    defer db.Close()

    // Open device
    handle, err = pcap.OpenLive(config.Device, snapshotLen, promiscuous, timeout)
    if err != nil {
        log.Fatal(err)
    }
    defer handle.Close()

    packetSource := gopacket.NewPacketSource(handle, handle.LinkType())

    for packet := range packetSource.Packets() {
         getPacketInfo(packet, db)
    }
}

func ReadConfig(file string) (*Config) {
    cFile, _      := os.Open(file)
    decoder       := json.NewDecoder(cFile)
    configuration := &Config{}
    err            = decoder.Decode(&configuration)
    checkErr(err)

    return configuration
}

func OpenDB(config *Config) (db *sql.DB) {
    dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", config.DBUser, config.DBPass, config.DBName)
    db, err := sql.Open("postgres", dbinfo)
    checkErr(err)

    return db
}

func getPacketInfo(packet gopacket.Packet, db *sql.DB) {
    // Let's see if the packet is an ethernet packet
    ethernetLayer := packet.Layer(layers.LayerTypeEthernet)
    if (ethernetLayer == nil) {
      return
    }

    payloadSize := len(packet.Data())

    timestmp := time.Now().UTC()

    // Let's see if the packet is IP (even though the ether type told us)
    ipLayer := packet.Layer(layers.LayerTypeIPv4)
    if (ipLayer == nil) {
      return
    }
    ip, _ := ipLayer.(*layers.IPv4)

    fromAddr := ip.SrcIP
    destAddr := ip.DstIP

    // Let's see if the packet is TCP
    tcpLayer := packet.Layer(layers.LayerTypeTCP)
    udpLayer := packet.Layer(layers.LayerTypeUDP)
    fromPort := 0
    destPort := 0
    if tcpLayer != nil {
        tcp, _ := tcpLayer.(*layers.TCP)
        fromPort = int(tcp.SrcPort)
        destPort = int(tcp.DstPort)
    } else
    // Let's see if the packet is UDP
    if (udpLayer != nil) {
        udp, _ := udpLayer.(*layers.UDP)
        fromPort = int(udp.SrcPort)
        destPort = int(udp.DstPort)
    }

    var lastInsertId int
    err = db.QueryRow(
      "INSERT INTO data(fromaddress, fromport, toaddress, toport, size, time) VALUES($1, $2, $3, $4, $5, $6) returning id;",
      fromAddr.String(), int(fromPort), destAddr.String(), int(destPort), int(payloadSize), timestmp.Format("2006-01-02 15:04:05")).Scan(&lastInsertId)
    checkErr(err)
}

func checkErr(err error) {
    if err != nil {
        panic(err)
    }
}
