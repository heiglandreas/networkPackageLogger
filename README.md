# Network Package Logger

A tool to log metadata from network-packages to a database for later analysis.

## Use Case

The tool reads packages from a network interface and writes date, source and destination IP and Port and the package size to a database.

The data can then be retrieved from that database for later analysis and statistics. Packages without a payload (AKA no size) will not be logged.

## Installation

Download the source-Code and compile it using ```go build```

## Configuration

Copy the `networkPackageLogger.json.dist` file to `networkPackageLogger.json` and
adapt the content appropriately.

Aditionaly you'll need a PostgreSQL-Database that needs to be initialized with this script:

```sql
CREATE SEQUENCE data_id_seq;

CREATE TABLE data (
    id bigint DEFAULT nextval('data_id_seq'),
    fromaddress character varying(15),
    fromport integer,
    toaddress character varying(15),
    toport integer,
    size integer,
    time timestamp without time zone
);
```

## Usage

* Add your configuration values to the config-file
* Move the config-file to your working path
* Call the binary from the working path using sudo like this: `sudo networkPackageLogger`
* The script now logs all incoming and outgoing traffic on the configured network-interface
  to the database.

You can now get statistics from the database using SQL-Statements like this:

```sql
    SELECT CAST (sum(size) AS FLOAT)/(1024) as KB, toAddress FROM data WHERE toAddress LIKE '172.%' GROUP BY toAddress;
```
